# -*- coding: utf-8 -*-
"""
Wyatt Conner
ME-305-01
Created on Thu Sep 23 15:45:19 2021
last updated: Sep 24 2021
"""

# Define a function to calculate the fibonacci number for a given index number. 
def fib (idx):
    # Define the first 2 fibonacci numbers as they are implied and are not
    # calculated. There is a total of 3 variables for calculations reasons.
    fibnum = [0, 1, 0]

    # The for loop is running from 2 since the first 2 numbers are defined
    # to plus one of the index number the user provided. 
    for x in range (2,idx+1):   
      
            # We are using the equation f(n) = f(n-1) + f(n-2) to calculate the 
            # fibonnaci number. 
            fibnum[2] = fibnum[1] + fibnum[0] 
            
            # Fibonnaci numbers we are going update our array every iteration
            # because we need to save the previous two. This is the reason why
            # the array has a length of 3. 
            fibnum [0] = fibnum[1]
            fibnum [1] = fibnum[2]
            
            # this just adds 1 to x every iteration to ensure the for loop
            # will move along properly. 
            x += 1
    
    # The return function defines the output of our function, so when we call
    # later the program knows what value to give back. In this case the output
    # is the last number in our array which is the fibonnacci number for the 
    # corresponding index number.         
    return fibnum[2]

# We define stop as we need the variable to exist before loop, because there
# is no reason to have its value update every iteration. 
stop = 'null'

# The while loop runs the section of program that interacts with the user
# this is defined to occur as long as the variable "stop" does not equal q.
# this will allow to user to stop the program by pressing q when prompted. 
while stop != 'q': 
    
    # This defines the library of this section of code. 
    if __name__ == '__main__':
        
        # The program will first try to find and interger value of from the
        # input of the user, but if its a string it will fail and then execute
        # the except portion of code prompting the user to enter an interger
        # and ends this iteration of the loop. 
        try:
            idx = int(input('Please enter a Fibonacci Index number: '))
        except:
            print('Enter an Interger Please.')
            continue
    # If the input the user put is negative it is impossible to have a negative
    # index number for a fibonacci number so we prompt the user and restart the 
    # iteration of the loop.     
    if idx < 0:
        print('Invalid Input: No negative Index numbers')
        continue
    
    # elif is a second if after our intial that will handle if a 0 was entered
    # by user as there is no need for the function as we define the first two
    # numbers in the sequence. This is same for our next elif for 1. 
    elif idx == 0:
        print(' The fibonacci number is 0')  
        
    elif idx == 1:
        print(' The fibonacci number is 1') 
    
    # else is used at the end if the user has put in a correct interger that is
    # greater than 1 this code will execute and call the function to calculate
    # the fibonacci number for that given index and output the number to the
    # user. 
    else:
             answer = fib(idx)
             output_string = 'The fibonacci number is'
             print(output_string, answer)
             
    # This is a prompt by the user to end to program by defining stop equal to 
    # zero to meet the end condition for the while loop of the program. 
    stop = input('enter q to stop the program: ')